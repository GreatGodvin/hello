#include "welcome.h"
#include "help.h"
#include "version.h"
#include <libhello.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define BASE10 10
void thankyou()
{
	printf("Thank you for using my program.\n");
}
void check_if_int(char *num)
{
	char *endptr = malloc(sizeof(endptr));
	long b = strtol(num, &endptr, BASE10);
	int a = (int)b;
	if (*endptr != '\n' || a == 0) {
		printf("You entered a non integer value\n");
		exit(-1);
	}
}
int main(int argc, char *argv[])
{
	if ((argc >= 2) && (strcmp(argv[1], "--version")) == 0)
		version();
	else if ((argc >= 2) && (strcmp(argv[1], "--help")) == 0)
		help();
	else {
		welcome();
		char *num1 = malloc(sizeof(*num1));
		char *num2 = malloc(sizeof(*num2));
		printf("Enter a number:");
		fgets(num1, sizeof(num1), stdin);
		long num1long = strtol(num1, NULL, BASE10);
		int num1int = (int)num1long;
		check_if_int(num1);
		printf("Enter another number: ");
		fgets(num2, sizeof(num2), stdin);
		long num2long = strtol(num2, NULL, BASE10);
		int num2int = (int)num2long;
		check_if_int(num2);
		printf("What operation do you want to do ? (add,subtract,divide,multiply): ");
		char *operation = malloc(sizeof(operation));
		if (scanf("%s", operation) != 1)
			printf("Failed to read input\n");
		if ((strcmp(operation, "add")) == 0) {
			int result = sum(num1int, num2int);
			printf("Sum is: %d\n", result);
		} else if ((strcmp(operation, "subtract")) == 0) {
			int result = difference(num1int, num2int);
			printf("Difference is: %d\n", result);
		} else if ((strcmp(operation, "multiply")) == 0) {
			int result = product(num1int, num2int);
			printf("Product is: %d\n", result);
		} else if ((strcmp(operation, "divide")) == 0) {
			int result = quotient(num1int, num2int);
			printf("Quotient is: %d\n", result);
		}
		char *no_thankyou = getenv("NO_THANKYOU");
		if ((no_thankyou) && (strcmp(no_thankyou, "1")) == 0)
			exit(1);
		else
			thankyou();
	}
}
