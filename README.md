# hello
hello is a simple [Hello World](https://en.wikipedia.org/wiki/%22Hello,_World!%22_program) program.

# Building
hello can be built using either meson or Flatpak.

### Building using meson
    meson _build
    meson compile -C _build

### Building using Flatpak
    flatpak-builder build-dir org.GreatGodvin.hello.json
    
# Running
If built using meson:

    ./_build/src/hello
If built using flatpak:

    flatpak-builder --run build-dir org.GreatGodvin.hello.json hello

# License
hello is licensed under the `GNU GENERAL PUBLIC LICENSE Version 3`.
